package uz.pdp.appintegrationtest.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class BookControllerTest {

    @Autowired
    private MockMvc mockMvc;

//    @MockBean
//    private BookService bookService;

    @Test
    public void addTest() throws Exception {

//        when(bookService.add(any())).thenReturn(new Book());

        MvcResult mvcResult = mockMvc.perform(
                        post("/api/book")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content("""
                                        {
                                          "title": "test_f251d2a63562",
                                          "author": "test_79ab60fa046d"
                                        }
                                        """)
                ).andExpect(status().is(201))
                .andReturn();
        System.out.println(mvcResult.getResponse().getContentAsString());
    }
}