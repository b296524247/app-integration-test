package uz.pdp.appintegrationtest.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import uz.pdp.appintegrationtest.entity.Book;
import uz.pdp.appintegrationtest.service.BookService;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
class BookControllerWebTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BookService bookService;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void addTest() throws Exception {

        Book book = new Book(1, "dadas", "test_79ab60fa046d");
        when(bookService.add(any())).thenReturn(book);

        String json = objectMapper.writeValueAsString(book);

        MvcResult mvcResult = mockMvc.perform(
                        post("/api/book")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(json)
                ).andExpect(status().is(201))
                .andReturn();
        String response = mvcResult.getResponse().getContentAsString();

        Book resBook = objectMapper.readValue(response, Book.class);

        assertThat(resBook).isNotNull();

        assertThat(resBook.getTitle()).isEqualTo(book.getTitle());
    }

    @Test
    public void addTestFail() throws Exception {

        Book book = new Book(1, null, "test_79ab60fa046d");
        when(bookService.add(any())).thenReturn(book);

        String json = objectMapper.writeValueAsString(book);

        MvcResult mvcResult = mockMvc.perform(
                        post("/api/book")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(json)
                ).andExpect(status().is(400))
                .andReturn();
    }
}