package uz.pdp.appintegrationtest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppIntegrationTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppIntegrationTestApplication.class, args);
    }

}
