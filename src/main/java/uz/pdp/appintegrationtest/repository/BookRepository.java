package uz.pdp.appintegrationtest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.pdp.appintegrationtest.entity.Book;

import java.util.Optional;

public interface BookRepository extends JpaRepository<Book, Integer> {

    @Query(nativeQuery = true, value = "SELECT * FROM book")
    Optional<Book> findByBla();
}