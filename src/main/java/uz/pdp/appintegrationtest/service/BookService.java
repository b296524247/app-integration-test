package uz.pdp.appintegrationtest.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import uz.pdp.appintegrationtest.entity.Book;
import uz.pdp.appintegrationtest.repository.BookRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class BookService {
    private final BookRepository bookRepository;


    public Book add(Book book) {
        return bookRepository.save(book);
    }

    public List<Book> list() {
        return null;
    }

    public Book one(Integer id) {
        return null;
    }
}
